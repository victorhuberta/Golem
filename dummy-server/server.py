#!/usr/bin/python3
 
from http.server import BaseHTTPRequestHandler, HTTPServer
from http.client import parse_headers
import urllib.parse as urlparse
import MySQLdb as mariadb
from _mysql_exceptions import Error as MySQLError
from hashlib import sha256

mariadb_conn = mariadb.connect(user='user', password='', database='test')
cursor = mariadb_conn.cursor()

login_query = 'SELECT * FROM users WHERE username="%s" AND password="%s"'
new_session_query = 'INSERT INTO sessions (sessionid, username) '+ \
    'VALUES ("%s", "%s");'
check_session_query = 'SELECT username FROM sessions WHERE sessionid="%s";'
drop_session_query = 'DELETE FROM sessions WHERE sessionid="%s";'
users_query = "SELECT %s FROM users WHERE usertype='%s';"
users_cols = ['id', 'username', 'usertype', 'occupation']
 
class VulnRequestHandler(BaseHTTPRequestHandler):
 
    def do_GET(self):
        req = decode_bytes(dict(urlparse.parse_qsl(self.path)))
        for key, val in req.items():
            req.pop(key)
            req[key.replace('/?', '')] = val

        cookie = to_dict(self.headers['Cookie']
            if self.headers.get('Cookie') else '') 

        self.send_response(200)
        self.send_header('Content-Type','text/html')
        self.end_headers()

        if req.get('usertype') is not None:
            with open('users.html', 'r') as users_page:
                users_template = users_page.read()

            try:
                cursor.execute(users_query % (','.join(users_cols),
                    req['usertype']))
            except MySQLError as e:
                content = users_template % (str(e), '')
                self.wfile.write(bytes(content, 'utf8'))
                return

            cols_html = ''
            for col in users_cols:
                cols_html += '<th>%s</th>' % col

            rows_html = ''
            for row in cursor:
                row_html = '<tr>'
                for col in row: row_html += '<td>%s</td>' % col
                row_html += '</tr>'
                rows_html += row_html

            content = users_template % (cols_html, rows_html)
            self.wfile.write(bytes(content, 'utf8'))
            return

        elif req.get('logout') == 'true':
            sessionid = cookie.get('sessionid')
            if sessionid is not '' and sessionid is not None:
                cursor.execute(drop_session_query % sessionid)
                mariadb_conn.commit()
                self.send_header('Set-Cookie', 'sessionid=') 

        else:
            sessionid = cookie.get('sessionid')
            if sessionid is not '' and sessionid is not None:
                cursor.execute(check_session_query % sessionid) 
                row = cursor.fetchone()
                if row is not None:
                    self.end_headers()
                    with open('profile.html', 'r') as profile_page:
                        content = profile_page.read() % ('Welcome ' + \
                            row[0] + '!')
                        self.wfile.write(bytes(content, 'utf8'))
                    return

        self.end_headers()
        with open('login.html', 'r') as login_page:
            self.wfile.write(bytes(login_page.read() % '', 'utf8'))


    def do_POST(self):
        content_len = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_len)
        req = decode_bytes(dict(urlparse.parse_qsl(post_data)))

        cursor.execute(login_query % (req['username'], req['password']))
        if (cursor.fetchone() is not None):
            sessionid = sha256((req['username']+req['password'])
                .encode('utf-8')).hexdigest()
            cursor.execute(check_session_query % sessionid)
            if cursor.fetchone() is None:
                cursor.execute(new_session_query % (sessionid, req['username']))
                mariadb_conn.commit()

            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.send_header('Logged-In', req['username'])
            self.send_header('Set-Cookie', 'sessionid='+sessionid)
            self.end_headers()

            with open('profile.html', 'r') as profile_page:
                content = profile_page.read() % ('Welcome ' + \
                    req['username'] + '!')
        else:
            self.send_response(200)
            self.send_header('Content-Type', 'text/html')
            self.end_headers()

            with open('login.html', 'r') as login_page:
                content = login_page.read() % 'Invalid login!'

        self.wfile.write(bytes(content, 'utf8'))


def decode_bytes(data):
    if isinstance(data, bytes): return data.decode('ascii')
    if isinstance(data, dict): return dict(map(decode_bytes, data.items()))
    if isinstance(data, tuple): return map(decode_bytes, data)
    return data


def to_dict(rawstr):
    if rawstr == '': return dict()
    return dict(item.split('=') for item in rawstr.split())

 
def run():
    print('starting server...')
 
    server_address = ('127.0.0.1', 8081)
    httpd = HTTPServer(server_address, VulnRequestHandler)
    print('running server...')
    httpd.serve_forever()
 
 
run()
