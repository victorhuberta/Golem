# Copyright (c) 2017 Victor Huberta

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import requests
from bs4 import BeautifulSoup
from difflib import SequenceMatcher
from .golem import GolemSingleton

class GolemCrawler(object):
    """Crawl target site, make requests, and receive responses."""

    def __init__(self):
        """Initialize a new object of GolemCrawler.
        
        Returns: None
        """
        self.golem = GolemSingleton.instance()
        self.matcher = SequenceMatcher()
        self.crawled_url = self.golem.get_target_url()
        self.sub_url = ''


    def recrawl(self, sub_url='', params={}, *, session=None):
        """(re)Crawl the target site under `sub_url`.

        Positional arguments:
        sub_url -- sub-URL for the target site
        params -- URL parameters

        Keyword arguments:
        session -- session object to be used (default None)

        Returns:
        session -- updated session/new session
        """
        self.crawled_url = self.golem.get_target_url() + sub_url
        if not self.crawled_url.endswith('/'):
            self.crawled_url += '/'
        self.sub_url = sub_url

        if session is None:
            session = self.new_session()

        r = session.get(self.crawled_url, params=params)
        self.resp = r
        self.soup = BeautifulSoup(r.text, 'html.parser')

        return session


    def new_session(self):
        """Request a new session object.

        Returns:
        session -- special session/normal session
        """
        if self.crawled_url.startswith('https://'):
            return self.golem.get_special_session()
        else:
            return requests.Session()


    def submit(self, web_form, *, session=None):
        """Make a POST request.

        Positional arguments:
        web_form -- WebForm object with form data

        Keyword arguments:
        session -- session object to be used (default None)

        Returns:
        session -- updated session/new session
        """
        action = web_form.get_action()
        form_data = web_form.get_form_data()

        if session is None:
            session = self.new_session()

        r = session.post(action, form_data)
        self.resp = r
        self.soup = BeautifulSoup(r.text, 'html.parser')

        return session


    def get_visible_urls(self):
        """Get all URLs in the crawled HTML page.

        Returns:
        visible_urls -- list of all found URLs
        """
        visible_urls = set()

        # Find URLs in `href` attribute (<a>, <link>).
        with_hrefs = self.soup.find_all('a')
        with_hrefs.extend(self.soup.find_all('link'))
        for with_href in with_hrefs:
            url = self._get_url(with_href, 'href')
            if url:
                visible_urls.add(url)

        # Find URLs in `src` attribute (<script>, <img>).
        with_srcs = self.soup.find_all('script')
        with_srcs.extend(self.soup.find_all('img'))
        for with_src in with_srcs:
            url = self._get_url(with_src, 'src')
            if url:
                visible_urls.add(url)

        # Find form action URLs.
        with_actions = self.soup.find_all('form')
        for with_action in with_actions:
            url = self._get_url(with_action, 'action')
            if url:
                visible_urls.add(url)

        return list(visible_urls)


    def get_crawled_url(self):
        return self.crawled_url


    def _get_url(self, elem, name):
        """Get full URL from HTML element.

        Positional arguments:
        elem -- HTML element
        name -- element attribute name

        Returns:
        url -- full URL
        """
        url = elem.get(name)
        if url and not url.startswith('http'):
            url = self.crawled_url + url
        return url


    def get_web_forms(self):
        """Get all web forms in crawled page.

        Returns:
        web_forms -- list of web forms
        """
        web_forms = []
        for form in self.soup.find_all('form'):
            web_form = WebForm(form)
            web_forms.append(web_form)
        return web_forms


    def find_web_form(self, form_id):
        """Find a form by form id.

        Positional arguments:
        form_id -- unique form id

        Returns:
        web_form -- a web form
        """
        for web_form in self.get_web_forms():
            if web_form.get_id() == form_id:
                return web_form

        return None


    def get_page_in_text(self):
        return self.soup.prettify()
    

    def get_strings(self):
        return list(self.soup.strings)


    def get_status_code(self):
        return self.resp.status_code


    def get_headers(self):
        return self.resp.headers


    def match_resps(self, params1, params2, sub_url1=None, sub_url2=None):
        """Check if the two crawled pages are the same.

        Positional arguments:
        params1 -- URL parameters for first page
        params2 -- URL parameters for second page
        sub_url1 -- sub-URL for first page (default None)
        sub_url2 -- sub-URL for second page (default None)

        Returns:
        match -- true if their similarity ratio is > 0.985
        """
        if sub_url1 is None:
            sub_url1 = self.sub_url
        if sub_url2 is None:
            sub_url2 = self.sub_url

        self.recrawl(sub_url1, params1)
        self.matcher.set_seq1(self.soup.prettify())

        self.recrawl(sub_url2, params2)
        self.matcher.set_seq2(self.soup.prettify())

        return self.matcher.ratio() > 0.985



class WebForm(object):
    """A wrapper for web form data."""

    def __init__(self, raw_form):
        """Initialize a new object of WebForm.

        Positional arguments:
        raw_form -- raw form provided by BeautifulSoup

        Returns: None
        """
        self.form = raw_form.attrs

        form_inputs = []
        for raw_form_input in raw_form.find_all('input'):
            form_inputs.append(raw_form_input.attrs)

        self.form['inputs'] = form_inputs


    def get(self, key):
        """Get a form input value by name.

        Positional arguments:
        key -- form input name

        Returns:
        value -- form input value
        """
        if key is None: return None

        for form_input in self.form['inputs']:
            if form_input['name'] == key:
                return form_input['value']


    def set(self, key, val):
        """Set a form input value by name.

        Positional arguments:
        key -- form input name
        val -- form input value

        Returns: None
        """
        if key is None: return

        for form_input in self.form['inputs']:
            if form_input.get('name') == key:
                form_input['value'] = val
                return


    def get_action(self):
        return self.form['action']
    

    def get_id(self):
        return self.form['id']


    def get_inputs(self):
        return self.form['inputs']
    

    def get_form_data(self):
        """Get web form data in a dict.

        Returns:
        form_data -- dict with web form data
        """
        form_data = {}

        for form_input in self.form['inputs']:
            name = form_input.get('name')
            if name is not None:
                form_data[name] = form_input.get('value')

        return form_data


    def __str__(self):
        return str(self.form)
