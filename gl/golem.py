# Copyright (c) 2017 Victor Huberta

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager
import ssl
from . import reporter

def init(target_url, target_port=80):
    """Initialize Golem singleton instance.

    Positional arguments:
    target_url -- test target URL
    target_port -- test target port (default 80)

    Returns:
    golem -- Golem singleton instance
    """
    return GolemSingleton.instance(target_url, target_port)


class SpecialAdapter(HTTPAdapter):
    """A special adapter class to be used for HTTPS purposes."""

    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections,
            maxsize=maxsize, block=block, ssl_version=ssl.PROTOCOL_TLSv1)


class Golem(object):
    """Perform target site related operations.
    Additionally, it contains target site information.
    """

    def __init__(self, target_url, target_port=80):
        """Initialize a new object of Golem.

        Positional arguments:
        target_url -- target site URL
        target_port -- target site port (default 80)

        Returns: None
        """
        self.target_url = target_url
        self.target_port = target_port

        if not self.target_url.startswith('http://') \
            and not self.target_url.startswith('https://'):
            self.target_url = 'http://' + target_url


    def get_target_url(self):
        target_url = self.target_url + ':' + str(self.target_port) + '/'
        return target_url


    def get_special_session(self):
        """Get a session object mounted with special adapter for HTTPS.

        Returns:
        s -- special session object
        """
        s = requests.Session()
        s.mount('https://', SpecialAdapter())
        return s


class GolemSingleton(object):
    """Manage the singleton object of Golem."""

    golem = None

    @staticmethod
    def instance(target_url='', target_port=80):
        """Get the singleton instance.

        Positional arguments:
        target_url -- target site URL (default '')
        target_port -- target site port (default 80)

        Returns:
        golem -- Golem singleton instance
        """
        try:
            if GolemSingleton.golem is None:
                GolemSingleton.golem = Golem(target_url, target_port)
        except Exception as e:
            raise GolemInitError(str(e))

        return GolemSingleton.golem


class GolemInitError(Exception):
    """Thrown when initialization of Golem fails."""
    pass


class GolemTestSuccess(Exception):
    """Thrown when a Golem test has a positive final result."""
    pass


class GolemTestFailure(Exception):
    """Thrown when a Golem test has a negative final result."""
    pass


def testcls(*, prefix='test'):
    """Save all functions in a class prefixed with `prefix`.
    The attribute `tests` contains a sorted list of all saved functions.

    Function name format:
    test[SORT NUM]_[TITLE]

    Examples:
    - test1_verify_password # Ran second
    - test0_check_something # Ran first

    Keyword arguments:
    prefix -- test function prefix (default 'test')

    Returns:
    testmeta -- a replacement class
    """
    class testmeta(type):
        """Replacement class for cls."""

        def __new__(cls, clsname, bases, clsdict):

            # Save functions prefixed with `prefix'.
            clsdict['tests'] = []
            for name, val in clsdict.items():
                if name.startswith(prefix) and callable(val):
                    clsdict['tests'].append(val)

            # Sort functions based on their names.
            clsdict['tests'] = sorted(clsdict['tests'],
                key=lambda f: int(f.__name__.split('_')[0].split(prefix)[1]))

            return super().__new__(cls, clsname, bases, clsdict)

    return testmeta


class GolemAbuseCase(metaclass=testcls()):
    """A base class for all Golem abuse cases."""

    def run_tests(self):
        """Run all test functions saved in self.tests.

        Returns: None
        """
        reporter.section_title('Running %s tests...' % self.__class__.__name__)
        reporter.long_boundary()

        for test in self.tests:
            try:
                test_title = ' '.join(test.__name__.split('_')[1:]).title()
                reporter.test_title(test_title)
                test(self)
            except GolemTestFailure:
                reporter.failure('!! TEST FAILED !!')
            except GolemTestSuccess:
                reporter.success('== TEST SUCCESSFUL ==')

        reporter.long_boundary()
