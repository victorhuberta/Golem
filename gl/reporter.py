# Copyright (c) 2017 Victor Huberta

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

from termcolor import cprint

def section_title(title):
    """Print an underlined string in yellow.

    Positional arguments:
    title -- title string

    Returns: None
    """
    cprint('')
    cprint(title, 'yellow', attrs=['underline'])


def long_boundary():
    """Print 80 spaces highlighted in grey."""
    cprint(' ' * 80, 'white', 'on_grey')


def test_title(title):
    """Print a bolded white string highlighted in blue.
    The string is prefixed by [TEST].

    Positional arguments:
    title -- title string

    Returns: None
    """
    cprint('')
    cprint('[TEST] %s' % title, 'white', 'on_blue', attrs=['bold'])


def subtest_title(title):
    """Print a white string highlighted in blue.
    The string is prefixed by [SUB TEST].

    Positional arguments:
    title -- title string

    Returns: None
    """
    cprint('')
    cprint('[SUB TEST] %s' % title, 'white', 'on_blue')


def say(text):
    """Print a white string."""
    cprint(text, 'white')


def small_failure(text):
    """Print a magenta string highlighted in white."""
    cprint(text, 'magenta', 'on_white')


def small_success(text):
    """Print a cyan string."""
    cprint(text, 'cyan')


def failure(text):
    """Print a bolded red string in uppercase."""
    cprint(text.upper(), 'red', attrs=['bold'])


def success(text):
    """Print a bolded green string in uppercase."""
    cprint(text.upper(), 'green', attrs=['bold'])
