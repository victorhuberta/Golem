# Copyright (c) 2017 Victor Huberta

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.

import json
from ..golem import GolemAbuseCase, GolemTestSuccess, GolemTestFailure
from ..crawler import GolemCrawler
from .. import reporter

class AccessControlAbuseCase(GolemAbuseCase):
    """An abuse case for testing access violation."""

    def __init__(self, *, privileges=[], access_groups=[]):
        """Initialize a new object of AccessControlAbuseCase.

        Keyword arguments:
        privileges -- list of access privileges to be tested
        access_groups -- list of access groups to be tested

        Returns: None
        """
        self.crawler = GolemCrawler()
        self.privileges = privileges
        self.access_groups = access_groups


    def test0_exclusive_privileges_of_users(self):
        """Every user should have their own private page.
        If any of the other users can get the same response as
        the user, there's an access violation.

        Returns: None
        """
        for privilege in self.privileges:
            user = privilege.get_user()
            rules_dict = privilege.get_rules().to_dict()
            reporter.subtest_title('Exclusive Privilege of User "%s"' %
                user.get_uid())

            for rule_name, (req, resp) in rules_dict.items():
                # Check the privilege of the user.
                if not self._check_privilege_rule(rule_name, user, req, resp):
                    raise GolemTestFailure()

                # Check the privilege of other users.
                for other_privilege in self.privileges:
                    other_user = other_privilege.get_user()
                    if other_user.get_uid() == user.get_uid():
                        continue

                    if self._check_privilege_rule(rule_name, other_user,
                        req, resp):
                        raise GolemTestFailure()

        raise GolemTestSuccess()


    def test1_privileges_of_access_groups(self):
        """Every user in an access group can visit a particular page.
        If any of the access group's users fail to get the same response,
        the test has failed.

        Returns: None
        """
        for access_group in self.access_groups:
            reporter.subtest_title('Privilege of Group "%s"' %
                access_group.get_gid())

            for user in access_group.get_users():
                rules_dict = access_group.get_rules().to_dict()

                for rule_name, (req, resp) in rules_dict.items():
                    if not self._check_privilege_rule(
                        rule_name, user, req, resp):
                        raise GolemTestFailure()

        raise GolemTestSuccess()


    def _check_privilege_rule(self, rule_name, user, req, resp):
        """Check if the response received by a user is as expected.
        Two responses are equal if the following are equal:
        * status codes
        * headers
        * identifying strings on pages

        Positional arguments:
        rule_name -- access privilege rule name
        user -- access user
        req -- sample request
        resp -- expected response

        Returns:
        everything_ok -- True if the response is as expected
        """
        reporter.say('Checking privilege rule "%s" for user %s...' 
            % (rule_name, user.get_uid()))
        reporter.say('Method %s to sub-URL "%s"' % (req.get_method(),
            req.get_sub_url()))

        if req.get_method() == AccessRequest.GET:
            self.crawler.recrawl(req.get_sub_url(), req.get_params(),
                session=user.get_session())
        elif req.get_method() == AccessRequest.POST:
            self.crawler.submit(req.get_web_form(),
                session=user.get_session())

        everything_ok = \
            self._verify_with('status_code',
                self._verify_status_code, resp) and \
            self._verify_with('headers',
                self._verify_headers, resp) and \
            self._verify_with('page texts',
                self._verify_page_text, resp)

        return everything_ok


    def _verify_with(self, target_name, verify_func, resp):
        """Verify that a response is as expected with verify_func().

        Positional arguments:
        target_name -- attribute to be verified
        verify_func -- verification function
        resp -- expected response

        Returns:
        ok -- True if it's successfully verified.
        """
        ok = verify_func(resp)
        if ok:
            reporter.small_success('%s: correct' % target_name)
        else:
            reporter.small_failure('%s: incorrect' % target_name)
        return ok


    def _verify_status_code(self, resp):
        """Verify that the status code is as expected.

        Positional arguments:
        resp -- expected response

        Returns:
        ok -- True if the status codes are equal
        """
        return self.crawler.get_status_code() == resp.get_status_code()
    

    def _verify_headers(self, resp):
        """Verify that the header is as expected.

        Positional arguments:
        resp -- expected response

        Returns:
        ok -- True if the headers are equal
        """
        headers1 = self.crawler.get_headers()
        headers2 = resp.get_headers()

        for var, val in headers2.items():
            if headers1.get(var) is None or headers1[var] != val:
                return False

        return True


    def _verify_page_text(self, resp):
        """Verify that the identifying strings are in the received response.

        Positional arguments:
        resp -- expected response

        Returns:
        ok -- True if the identifying strings were found
        """
        for page_text in resp.get_page_texts():
            if self.crawler.get_page_in_text().find(page_text) == -1:
                return False

        return True


class AccessPrivilege(object):
    """Access privilege description for users."""
    
    def __init__(self, user, rules):
        """Initialize a new object of AccessPrivilege.

        Positional arguments:
        user -- access user
        rules -- access rules

        Returns: None
        """
        self.user = user
        self.rules = rules


    @staticmethod
    def parse_json(filename):
        """Parse a JSON file which defines a user's access privileges.

        Positional arguments:
        filename -- JSON file path

        Returns:
        priv -- user's access privilege object
        """
        with open(filename, 'r') as json_file:
            priv_data = json.load(json_file)
            user = AccessUser(priv_data['user']['id'])
            user.login(**priv_data['user']['session'])

            rules = AccessRules()
            rules_data = priv_data['rules']

            for rule_name, rule in rules_data.items():
                req = AccessRequest(**rule['request'])
                resp = AccessResponse(**rule['response'])
                rules.set_rule(rule_name, req, resp)

            return AccessPrivilege(user, rules)


    def get_user(self):
        return self.user


    def set_user(self, user):
        self.user = user


    def get_rules(self):
        return self.rules


class AccessGroup(object):
    """Defines an access group."""

    def __init__(self, group_id, users, rules):
        """Initialize a new object of AccessGroup.

        Positional arguments:
        group_id -- access group's name or ID
        users -- list of users in access group
        rules -- list of access rules for the group

        Returns: None
        """
        self.group_id = group_id
        self.users = users
        self.rules = rules


    def parse_json(filename):
        """Parse a JSON file which defines an access group's privileges.

        Positional arguments:
        filename -- JSON file path

        Returns:
        priv -- access group object
        """
        with open(filename, 'r') as json_file:
            grp_data = json.load(json_file)
            gid = grp_data['group_id']

            users = []
            for user_data in grp_data['users']:
                user = AccessUser(user_data['id'])
                user.login(**user_data['session'])
                users.append(user)

            rules = AccessRules()
            rules_data = grp_data['rules']

            for rule_name, rule in rules_data.items():
                req = AccessRequest(**rule['request'])
                resp = AccessResponse(**rule['response'])
                rules.set_rule(rule_name, req, resp)

            return AccessGroup(gid, users, rules)


    def get_gid(self):
        return self.group_id


    def get_users(self):
        return self.users


    def get_rules(self):
        return self.rules


class AccessUser(object):
    """Defines an access user."""

    def __init__(self, user_id):
        """Initialize a new object of AccessUser.

        Positional arguments:
        user_id -- user's name or ID

        Returns: None
        """
        self.user_id = user_id
        self.session = None


    def get_uid(self):
        return self.user_id


    def get_session(self):
        return self.session


    def login(self, *, sub_url='', params={}, form_id, creds):
        """Authenticate the user and associate them with the new session.

        Keyword arguments:
        sub_url -- authentication sub-URL (default '')
        params -- authentication URL parameters (default {})
        form_id -- form ID on the page
        creds -- user's credentials

        Returns: None
        """
        crawler = GolemCrawler()
        crawler.recrawl(sub_url, params)

        web_form = crawler.find_web_form(form_id)
        if web_form is not None:
            # Input credentials into the form.
            for input_name, input_val in creds.items():
                web_form.set(input_name, input_val)

            self.session = crawler.submit(web_form)


class AccessRules(object):
    """Defines a list of access rules."""

    def __init__(self, rules_dict=None):
        """Initialize a new object of AccessRules.

        Positional arguments:
        rules_dict -- dictionary of access rules (default None)

        Returns: None
        """
        if rules_dict is None:
            rules_dict = {}
        self.rules_dict = rules_dict


    def set_rule(self, rule_name, req, resp):
        self.rules_dict[rule_name] = (req, resp)


    def replace_rules(self, rules_dict):
        self.rules_dict = rules_dict


    def to_dict(self):
        return self.rules_dict


class AccessRequest(object):
    """Defines a sample access request."""

    GET = "GET"
    POST = "POST"

    def __init__(self, *, method, sub_url='', params=None, web_form=None):
        """Initialize a new object of AccessRequest.

        Keyword arguments:
        method -- HTTP method to be used
        sub_url -- sub-URL to be used (default '')
        params -- sample URL parameters (default None)
        web_form -- form to be submitted (default None)

        Returns: None
        """
        if params is None:
            params = {}

        self.method = method
        self.sub_url = sub_url
        self.params = params
        self.web_form = web_form


    def get_method(self):
        return self.method
    

    def get_sub_url(self):
        return self.sub_url


    def get_params(self):
        return self.params
    

    def get_web_form(self):
        return self.web_form


class AccessResponse(object):
    """Defines the expected response."""

    def __init__(self, *, status_code, headers=None, page_texts=[]):
        """Initialize a new object of AccessResponse.

        Keyword arguments:
        status_code -- expected status code
        headers -- expected headers (default None)
        page_texts -- identifying strings on page (default [])

        Returns: None
        """
        if headers is None:
            headers = {}

        self.status_code = status_code
        self.headers = headers
        self.page_texts = page_texts


    def get_status_code(self):
        return self.status_code


    def get_headers(self):
        return self.headers


    def get_page_texts(self):
        return self.page_texts
