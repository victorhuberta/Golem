from gl import golem
from gl.golem import GolemInitError
from gl.security.accesscontrol import *

def main():
    try:
        golem.init('http://localhost', 8081)
    except GolemInitError as e:
        print(e)
        return

    privilege1 = AccessPrivilege.parse_json('demo_priv1.json')
    privilege2 = AccessPrivilege.parse_json('demo_priv2.json')
    privileges = [privilege1, privilege2]

    access_group = AccessGroup.parse_json('demo_accgrp1.json')
    ac = AccessControlAbuseCase(privileges=privileges,
        access_groups=[access_group])
    ac.run_tests()


if __name__ == '__main__':
    main()
