import sys
from gl import golem
from gl.golem import Golem, GolemInitError
from gl.security import sqlinjection as sqli

def main():
    try:
        golem.init('http://localhost', 8081)
    except GolemInitError as e:
        print(e)
        sys.exit(1)

    sqlcase = sqli.SQLiAbuseCase(sample_params={'usertype': 'public'})
    sqlcase.run_tests()


if __name__ == '__main__':
    main()
